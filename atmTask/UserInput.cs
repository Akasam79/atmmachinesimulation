﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atmTask
{
    class UserInput
    {
        public static void UserInputAccountsMenu(out int userInput)
        {
            bool isRunning = true;
            userInput = 0;
            while (isRunning == true)
            {
                bool isGoodNumber = int.TryParse(Console.ReadLine(), out userInput);
                if (isGoodNumber && (userInput == 0 || userInput == 1 || userInput == 2 || userInput == 3))
                {
                    isRunning = false;
                }
                else
                {
                    Console.Write("Incorrect Selection. Please Try Again: 0 ... 3: ");
                }
            }
        }

        public static void pinCheck (out int pin)
        {
            bool isRunnning = true;
            pin = 2250;
            while(isRunnning == true)
            {
                bool isGoodNumber = int.TryParse(Console.ReadLine(), out pin);
                if (isGoodNumber && pin == 2250)
                    {
                    isRunnning = false;
                    }
                else if (isGoodNumber && pin == 0)
                {
                    programExit.ExitATM();
                }
                else
                {
                    int LoginAttempts = 0;
                    for (int i = 0; i < 2; i++)
                    {
                        Console.WriteLine("Invalid entry, You'll be kicked out after 3 trials");
                        isGoodNumber = int.TryParse(Console.ReadLine(), out pin);
                        if (!isGoodNumber && pin != 2250)
                        LoginAttempts++;
                        else
                            break;
                        
                    }

                    if (LoginAttempts > 1)
                        programExit.ExitATM();
                }
                    
            }
        }
        

        public static bool UserInputYesNo(int mainMenuOptionSelected)
        {
            string[] accountAction = { "", "check the balance of", "withdraw from", "transfer to" };
            Console.Write("Would you like to {0} another account <Y or N> ? ", accountAction[mainMenuOptionSelected]);
            bool validInput = false;
            while (validInput == false)
            {
                string userChoice = Console.ReadLine();
                userChoice = userChoice.ToLower();
                switch (userChoice)
                {
                    case "y":
                        validInput = true;
                        Console.Clear();
                        Account.DisplayAccountsMenu(mainMenuOptionSelected);
                        return true;
                    case "n":
                        validInput = true;
                        Console.Clear();
                        return false;
                    default:
                        Console.Write("Incorrect selection, Please Try Again: <Y or N> ");
                        break;
                }
            }
            return false;
        }
    }
}
