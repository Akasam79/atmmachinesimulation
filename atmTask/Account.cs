﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atmTask
{
    class Account
    {
        const int SAVING_ACCOUNT = 1;
        const int CURRENT_ACCOUNT = 2;
        const int INVESTMENT_ACCOUNT = 3;

        public static double[] accountBalances = { 0.0, 323530.50, 34900.0, 2630000.0, 300000};

        public static string[] accountNames = { "", "Savings Account", "Current Account", "Investment Account" };


        public static void AccountsMenu(string mainMenuOptionSelected)
        {
            switch (mainMenuOptionSelected)
            {
                case "0":
                    programExit.ExitATM(); //0. Exit Program
                    break;
                case "1":
                    DisplayAccountsMenu(1); //1. Check Balance
                    AccountsActionMenu(1);
                    break;
                case "2":
                    DisplayAccountsMenu(2); //2. Withdrawal 
                    AccountsActionMenu(2);
                    break;
                case "3":
                    DisplayAccountsMenu(3); //3. Transfer
                    AccountsActionMenu(3);
                    break;
                default:
                    Console.Write("Incorrect selection, Please Try Again: 0 ... 3: ");
                    break;
            }

        }

        public static void DisplayAccountsMenu(int optionSelected)
        {
            Console.Clear();
            string[] menuOptions = { "", "Check Balance", "Withdrawal", "Transfer" };
            Console.Write("\n--------------------------------------------\n\n\tAccounts\n\t============\n");
            Console.WriteLine("\n\t{0}", menuOptions[optionSelected]);
            Console.WriteLine("\t1. {0}\n \t2. {1}\n \t3. {2}\n", accountNames[1], accountNames[2], accountNames[3]);

            if (optionSelected == 3)
            {
                Console.WriteLine("\nPlease select account to transfer from.");
            }
            Console.Write("Please Enter Your Choice 1 ... 3 or 0 to exit: ");
        }

        static void AccountsActionMenu(int mainMenuOptionSelected)
        {
            bool isRunning = true;
            while (isRunning == true)
            {
                int whichAccount;
                UserInput.UserInputAccountsMenu(out whichAccount);
                if (whichAccount == 0)
                {
                    isRunning = false;
                    break;
                }
                switch (mainMenuOptionSelected)
                {
                    case 1:
                        Balance.DisplayBalance(whichAccount);
                        isRunning = UserInput.UserInputYesNo(mainMenuOptionSelected);
                        break;
                    case 2:
                        Withdrawal.WithdrawAmount(whichAccount);
                        isRunning = UserInput.UserInputYesNo(mainMenuOptionSelected);
                        break;
                    case 3:
                        Transfer.TransferPromtMenu(whichAccount);
                        isRunning = UserInput.UserInputYesNo(mainMenuOptionSelected);
                        break;
                }
            }
        }
    }
}
