﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atmTask
{
  
    class Balance
    {
        public static void DisplayBalance(int whichAccount)
        {
            DateTime date = DateTime.Now;
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("\nCurrent Date: {0} ", date);
            Console.WriteLine("Balance of {0} is {1:C2}\n", Account.accountNames[whichAccount], Account.accountBalances[whichAccount]);
        }
    }
}
