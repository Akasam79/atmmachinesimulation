﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atmTask
{
    class MainMenu

    {

        public static string ShowMainMenu()
        {
            Console.Clear();
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("Welcome to Einstein's ATM.\n");
            Console.WriteLine("Please enter your pin to Proceed or 0 to terminate");
            bool isRunning = true;
            while(isRunning == true)
            {
                int userPin;
                UserInput.pinCheck(out userPin);
                if (userPin.Equals(null))
                {
                    isRunning = false;
                    break;
                }
                

                Console.WriteLine("\tTransaction Menu\n\t================");
                Console.WriteLine("\t1. Check Balance");
                Console.WriteLine("\t2. Withdrawal");
                Console.WriteLine("\t3. Transfer");
                Console.Write("\n\nPlease enter your choice 1 ... 3 or 0 to exit: ");
                break;

            }
            
            string menuOptionSelected = Console.ReadLine();
            return menuOptionSelected;

        }
    }
}
