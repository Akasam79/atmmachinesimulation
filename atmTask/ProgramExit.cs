﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atmTask
{
    class programExit
    {
        public static void ExitATM()
        {
            Console.Clear();
            Console.WriteLine("\n\tThank you for using Einstein's ATM, have a lovely day!");
            Console.WriteLine("\n\n\tPress any key to exit...");
            Console.ReadKey();
            Environment.Exit(1);
        }//end ExitATM
    }
}
