﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atmTask
{
    class Withdrawal
    {
       
        public static void WithdrawAmount(int whichAccount)
        {
            Console.WriteLine("\n--------------------------------------------");
            Console.Write("{0} account balance is {1:C2} \nPlease enter how much to withdraw from {0}, or 0 to exit: ", Account.accountNames[whichAccount], Account.accountBalances[whichAccount], Account.accountNames[whichAccount]);
            double amountToWithdraw = CashDispenser.UserTransactionCashAmount(whichAccount, 100);
            if (amountToWithdraw > 0)
            {
                CashDispenser.DispenseCash(amountToWithdraw, whichAccount);
            }
        }
    }
}
