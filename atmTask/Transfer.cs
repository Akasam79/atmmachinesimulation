﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atmTask
{
    class Transfer
    {

        public static void TransferPromtMenu(int fromAccount)
        {
            if (fromAccount != 0)
            {
                int toAccount;
                Console.Write("\nPlease Select the account to transfer to: ");
                Console.Write("\nPlease Enter Your Choice 1 ... 3 or 0 to exit: ");
                UserInput.UserInputAccountsMenu(out toAccount);
                if (fromAccount == toAccount)
                {
                    Console.Write("\nYou cannot transfer to the same account.\n");
                }
                else if (toAccount != 0)
                {
                    TransferAmount(fromAccount, toAccount);
                }
            }
        }

        /// <param name="fromAccount">account from which money is to withdrawn</param>
        /// <param name="toAccount">account to which money is to be transferred</param>
        static void TransferAmount(int fromAccount, int toAccount)
        {
            Console.Write("\n--------------------------------------------\n");
            Console.Write("Enter amount to transfer from {0} to {1}: $", Account.accountNames[fromAccount], Account.accountNames[toAccount]);
            double userAmountToTransfer;
            userAmountToTransfer = CashDispenser.UserTransactionCashAmount(fromAccount, 0);
            Account.accountBalances[fromAccount] = Account.accountBalances[fromAccount] - userAmountToTransfer;
            Account.accountBalances[toAccount] = Account.accountBalances[toAccount] + userAmountToTransfer;
            Console.WriteLine("Transfer was successful.");
            Balance.DisplayBalance(toAccount);
            Balance.DisplayBalance(fromAccount);
        }


    }
}
