﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atmTask
{
    class CashDispenser
    {
        public static void DispenseCash(double amountToWithdraw, int whichAccount)
        {
            int numberOfFiveHundredNotes = 0;
            int numberOfTwoHundredNotes = 0;
            double amountToWithdrawCalculator = amountToWithdraw;
            while (amountToWithdrawCalculator > 0)
            {
                if (amountToWithdraw > Account.accountBalances[4])
                {
                    Console.WriteLine("Temporarily Unable to dispense cash. pls withdraw a lower amount");
                    break;
                }
                if ((amountToWithdrawCalculator < 1000) && (amountToWithdrawCalculator > 500) && (amountToWithdrawCalculator % 200 == 0))
                {
                    numberOfTwoHundredNotes = (numberOfTwoHundredNotes + Convert.ToInt32(amountToWithdrawCalculator / 200));
                    amountToWithdrawCalculator = 0;
                }
                if (amountToWithdrawCalculator >= 500)
                {
                    amountToWithdrawCalculator = amountToWithdrawCalculator - 500;
                    numberOfFiveHundredNotes = numberOfFiveHundredNotes + 1;
                }
                if ((amountToWithdrawCalculator >= 200) && (amountToWithdrawCalculator < 500))
                {
                    amountToWithdrawCalculator = amountToWithdrawCalculator - 200;
                    numberOfTwoHundredNotes = numberOfTwoHundredNotes + 1;
                }
                if ((amountToWithdrawCalculator < 200) && (amountToWithdrawCalculator > 0))
                {
                    Console.WriteLine("Invalid amount specified. Unable to dispense in #200 and #500 notes.");
                    break;
                }
            }
            if (amountToWithdrawCalculator == 0)
            {
                Account.accountBalances[whichAccount] = (Account.accountBalances[whichAccount] - amountToWithdraw);
                Account.accountBalances[4] = (Account.accountBalances[4] - amountToWithdraw);
                Console.WriteLine("\n--------------------------------------------");
                Console.WriteLine("\nPlease collect {0:C2} consisting of: ", amountToWithdraw);
                Console.WriteLine("x{0} #5000 notes.", numberOfFiveHundredNotes);
                Console.WriteLine("x{0} #2000 notes.", numberOfTwoHundredNotes);
                if (whichAccount == 3)
                {
                    Account.accountBalances[whichAccount] = Account.accountBalances[whichAccount] - 10;
                    Console.WriteLine("\nA #10 transaction fee has been deducted for \nwithdrawing from your Investment Account.");
                }
                Balance.DisplayBalance(whichAccount);
            }
        }

        public static double UserTransactionCashAmount(int whichAccount, int transactionType)
        {
            bool isRunning = true;
            double userCashInput = 0;
            while (isRunning == true)
            {
                bool isGoodNumber = double.TryParse(Console.ReadLine(), out userCashInput);
                if (isGoodNumber && (userCashInput == 0))
                { // If input is 0 stop running
                    isRunning = false;
                }
                else if (isGoodNumber && userCashInput > 0)
                {
                    if ((whichAccount == 2) && ((Account.accountBalances[whichAccount] - userCashInput) < -20000))
                    {// Overdrawing the current account
                        Console.Write("\nYou cannot be more than #20000 in debt on your current account. \nPlease try another amount or 0 to cancel: ");
                    }
                    else if ((whichAccount == 2) && ((Account.accountBalances[whichAccount] - userCashInput) >= -20000))
                    { // Credit Withdraw
                        isRunning = false;
                        return userCashInput;
                    }
                    else if ((userCashInput > Account.accountBalances[whichAccount]))
                    { // Not enough in account for transaction
                        Console.Write("\nSorry, there are insufficent funds on the account. \nPlease try another amount or 0 to cancel:  ");
                    }
                    else if ((userCashInput <= Account.accountBalances[whichAccount] && ((Account.accountBalances[whichAccount] - userCashInput) < transactionType)))
                    { //Normal Account overdraw 
                        Console.Write("\nSorry, there are insufficent funds on the account. \nPlease try another amount or 0 to cancel: ");
                    }
                    else if ((userCashInput <= Account.accountBalances[whichAccount] && ((Account.accountBalances[whichAccount] - userCashInput) >= transactionType)))
                    { //Normal Account withdraw 
                        isRunning = false;
                        return userCashInput;
                    }
                }
                else
                {
                    Console.Write("\nSorry, that is not a valid amount. \nPlease try another amount or 0 to cancel: ", userCashInput);
                }
            }
            return 0;
        }//end UserInputAccountsMenu
    }
}
